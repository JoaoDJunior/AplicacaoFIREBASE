import { ViewChild, Component } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { GraficoPage } from '../pages/grafico/grafico';
import { PerfilPage } from '../pages/perfil/perfil';

import { LoginPage } from '../pages/login/login';
import { AngularFireAuth } from 'angularfire2/auth';
//import { ViewChild } from '@angular/core/src/metadata/di';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild (Nav) nav: Nav;
  rootPage:any = LoginPage;

  constructor(private menu: MenuController ,private afAuth: AngularFireAuth ,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  logout() {
		this.afAuth.auth.signOut().then(data => {
      this.nav.setRoot(LoginPage);
      //this.navCtrl.setRoot("LoginPage");
		}).catch(err=> {
			alert(err.message);
		})
  }

  GoToGraficoPage() {
    this.nav.push(GraficoPage);
    
  }

  GoToPerfilPage() {
    this.nav.push(PerfilPage);
  }

  



}

