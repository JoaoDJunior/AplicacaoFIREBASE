import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

//Chart.js
import { ChartsModule } from 'ng2-charts';


//PDFMaker
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { PerfilPage } from '../pages/perfil/perfil';
import { HomePage } from '../pages/home/home';
import { GraficoPage } from '../pages/grafico/grafico';
//import angular firebase
import {AngularFireModule, FirebaseAppConfig } from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireDatabase, AngularFireObject} from 'angularfire2/database';
import { FiredatabaseProvider } from '../providers/firedatabase/firedatabase';
import { DadosService } from '../services/plantacao/dados.service';
import { AngularFireAuth } from 'angularfire2/auth';



export const firebaseConfig : FirebaseAppConfig = {
  apiKey: "AIzaSyD0U-AoxlXSW4MtPIVTkcPX2N5utewiKTo",
  authDomain: "sisbjd-d5da6.firebaseapp.com",
  databaseURL: "https://sisbjd-d5da6.firebaseio.com",
  projectId: "sisbjd-d5da6",
  storageBucket: "sisbjd-d5da6.appspot.com",
  messagingSenderId: "599834856632"
};

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SignupPage,
    HomePage,
    PerfilPage,
    GraficoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    //AngularFirebase
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    //Chart.js
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    HomePage,
    PerfilPage,
    GraficoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FiredatabaseProvider,
    DadosService,
    File,
    FileOpener
  ]
})
export class AppModule {}
