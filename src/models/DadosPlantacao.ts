export interface DadosPlantacao {

    key?: string;
    tipo: string;
    valor: number;
    data: Date;

}