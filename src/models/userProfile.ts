export interface Profile {
    primeiroNome: string;
    segundoNome: string;
    email: string;
    fone: Number;
}