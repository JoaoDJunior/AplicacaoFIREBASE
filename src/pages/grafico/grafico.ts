import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { DadosPlantacao } from '../../models/DadosPlantacao';
import { DadosService } from '../../services/plantacao/dados.service';
import { Observable } from 'rxjs/Observable';
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { Profile } from '../../models/userProfile';
import { ToastCmp } from 'ionic-angular/components/toast/toast-component';

import { Chart } from 'chart.js';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { AngularFireAuth } from 'angularfire2/auth';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
/**
 * Generated class for the GraficoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grafico',
  templateUrl: 'grafico.html',
})
export class GraficoPage implements OnInit {

  @ViewChild('lineChart') lineChart;

  public plantacaoDados: any = {"":[]};

  public lineChartElement: any;

  public chartLabels: any = [];
  public chartValues: any = [];
  public chartColours: any = [];
  public chartHoverColours: any = [];

  userData: AngularFireObject<Profile>;

  temperatura: DadosPlantacao = {
    tipo:'temperatura',
    valor: undefined,
    data: undefined
  };
  umidadeAr: DadosPlantacao = {
    tipo:'umidade_de_ar',
    valor: undefined,
    data: undefined
  };
  umidadeSolo: DadosPlantacao = {
    tipo:'umidade_de_solo',
    valor: undefined,
    data: undefined,
  };

  temperaturaList$: Observable<DadosPlantacao[]>;

  dadosPLantacao: DadosPlantacao[];

  umidadeArList$: Observable<DadosPlantacao[]>;

  umidadeSoloList$: Observable<DadosPlantacao[]>;

  private userId: string = '';

  private temperaturaRef;

  private umidadeArRef;

  private umidadeSoloRef;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private afDatabase: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private toast: ToastController) {
    
      this.afAuth.authState.subscribe(dados => {
        if( dados && dados.email && dados.uid) {
          this.toast.create({
            message: `Bem vindo, ${dados.email}`,
            duration: 3000
          }).present();
          this.userData = this.afDatabase.object(`users/${dados.uid}`);
          this.userId = dados.uid;
          console.log(this.userId);
  
          this.temperaturaList$ = this.getTemperatura()         //Busca uma lista de temperaturas
          .snapshotChanges()        //Chave e valores
          .map(changes => {
          return changes.map(c => ({
            key: c.payload.key, ...c.payload.val()
          }));
        });

        console.log(this.temperaturaList$);
  
        this.umidadeArList$ = this.getUmidadeAr()
        .snapshotChanges()
        .map(changes => {
          return changes.map(c => ({
            key: c.payload.key, ...c.payload.val()
          }));
        });
  
        this.umidadeSoloList$ = this.getUmidadeSolo()
        .snapshotChanges()
        .map(changes => {
          return changes.map(c => ({
            key: c.payload.key, ...c.payload.val()
          }));
        });
  
        } else {
          this.toast.create({
          message: `Você não esta logado!`,
          duration: 3000
          }).present();
        }
      });
  

  }

  ngOnInit() {
    
    //this.basicChart();
  }

  ionViewDidLoad() {
    this.definirDados();
    this.criarGrafico();
  }

  definirDados() {
    this.afDatabase.list<DadosPlantacao>(`users/${this.userId}/plantacao/temperatura`)
    .valueChanges()
    .map(res =>{
      return res.map( valor => this.chartLabels);
    })
    .subscribe(temp => {
        this.chartLabels.push(temp);
         //this.chartValues.push();
         console.log(this.chartLabels);
         console.log(this.chartValues);
         this.criarGrafico();
    })
    //this.criarGrafico();
  }

  getTemperatura() {
    this.temperaturaRef = this.afDatabase.list<DadosPlantacao>(`users/${this.userId}/plantacao/temperatura`);
    return this.temperaturaRef;
  }

  getUmidadeAr() {
    this.umidadeArRef = this.afDatabase.list<DadosPlantacao>(`users/${this.userId}/plantacao/umidade_do_ar`);
    return this.umidadeArRef;
  }

  getUmidadeSolo() {
    this.umidadeSoloRef = this.afDatabase.list<DadosPlantacao>(`users/${this.userId}/plantacao/umidade_do_solo`);
    return this.umidadeSoloRef;
  }

  criarGrafico() {
    this.lineChartElement 		  = new Chart(this.lineChart.nativeElement,
      {
         type: 'line',
         data: {
            labels: this.chartLabels,
            datasets: [{
               label                 : 'Daily Technology usage',
               data                  : this.chartValues,
               duration              : 2000,
               easing                : 'easeInQuart',
               backgroundColor       : this.chartColours,
               hoverBackgroundColor  : this.chartHoverColours,
               fill 				   : false
            }]
         },
         options : {
            maintainAspectRatio: false,
            legend         : {
               display     : true,
               boxWidth    : 80,
               fontSize    : 15,
               padding     : 0
            },
            scales: {
               yAxes: [{
                  ticks: {
                     beginAtZero:true,
                     stepSize: 5,
                     max : 100
                  }
               }],
               xAxes: [{
                  ticks: {
                     autoSkip: false
                  }
               }]
            }
         }
      });
  }
}
