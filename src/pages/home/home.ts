import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { ToastCmp } from 'ionic-angular/components/toast/toast-component';
import { Profile } from '../../models/userProfile';
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { App } from 'ionic-angular/components/app/app';
import { DadosPlantacao } from '../../models/DadosPlantacao';
import { DadosService } from '../../services/plantacao/dados.service';
import { Observable } from 'rxjs/Observable';
import { ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { Console } from '@angular/core/src/console';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild('lineCanvas') lineCanvas;

  public temperaturaNow: Observable<String>;

  public temperaturaNow$: Observable<String>;

  public umidadeArNow: Observable<String>;

  public umidadeArNow$: Observable<String>;

  public umidadeSoloNow: Observable<String>;

  public umidadeSoloNow$: Observable<String>;

  public releNow: Observable<boolean>;

  private lineChart: any;

  private chartLabels: any = [];
  private chartValues: any = [];

  userData: AngularFireObject<Profile>;

  temperatura: DadosPlantacao = {
    tipo:'temperatura',
    valor: undefined,
    data: undefined
  };
  umidadeAr: DadosPlantacao = {
    tipo:'umidade_de_ar',
    valor: undefined,
    data: undefined
  };
  umidadeSolo: DadosPlantacao = {
    tipo:'umidade_de_solo',
    valor: undefined,
    data: undefined,
  };

  temperaturaList$: Observable<DadosPlantacao[]>;

  dadosPLantacao: DadosPlantacao[];

  umidadeArList$: Observable<DadosPlantacao[]>;

  umidadeSoloList$: Observable<DadosPlantacao[]>;

  private userId: string = '';

  private temperaturaRef;

  private umidadeArRef;

  private umidadeSoloRef;

  private releRef;

  private temperaturaNowRef;

  private umidadeArNowRef;

  private umidadeSoloNowRef;

  constructor(private afDatabase: AngularFireDatabase,
              public app:App, public menuCtrl: MenuController,
              public navCtrl: NavController,
              public navParams: NavParams,
              private toast: ToastController,
              public afAuth: AngularFireAuth,
              private dadosService: DadosService) {
    
    this.menuCtrl.enable(true);

    this.afAuth.authState.subscribe(dados => {
      if( dados && dados.email && dados.uid) {
        this.toast.create({
          message: `Bem vindo, ${dados.email}`,
          duration: 3000
        }).present();
        this.userData = this.afDatabase.object(`users/${dados.uid}`);
        this.userId = dados.uid;
        console.log(this.userId);

        this.temperaturaList$ = this.getTemperatura()         //Busca uma lista de temperaturas
        .snapshotChanges()        //Chave e valores
        .map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      });

      this.umidadeArList$ = this.getUmidadeAr()
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      });

      this.umidadeSoloList$ = this.getUmidadeSolo()
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      });

      this.temperaturaNow$ = this.buscaTemperaturaNow()
      .snapshotChanges()
      .subscribe(ref => {
      console.log(ref.payload.val())
      this.temperaturaNow = ref.payload.val();
      return ref.payload.val();
      });

      this.umidadeArNow$ = this.buscaUmidadeArNow()
      .snapshotChanges()
      .subscribe(ref => {
        this.umidadeArNow = ref.payload.val();
        return ref.payload.val();
      });

      this.umidadeSoloNow$ = this.buscaUmidadeSoloNow()
      .snapshotChanges()
      .subscribe(ref => {
        this.umidadeSoloNow = ref.payload.val();
        return ref.payload.val();
      });

      } else {
        this.toast.create({
        message: `Você não esta logado!`,
        duration: 3000
        }).present();
      }
    });
    
  }
  
  
  ionViewDidLoad() {
    this.DefinirDadosGrafico();
  }

  addTemperatura(umidadeSolo: DadosPlantacao) {
    this.umidadeSoloRef.push(umidadeSolo).then( ref => {
      console.log(ref.key);
    });
  }
  //Serve para atualizar dados em tempo real e disponibilizar para o usuario
  buscaDadosAtuais() {

  }

  getTemperatura() {
    this.temperaturaRef = this.afDatabase.list<DadosPlantacao>(`users/${this.userId}/plantacao/temperatura`);
    return this.temperaturaRef;
  }

  getUmidadeAr() {
    this.umidadeArRef = this.afDatabase.list<DadosPlantacao>(`users/${this.userId}/plantacao/umidade_do_ar`);
    return this.umidadeArRef;
  }

  getUmidadeSolo() {
    this.umidadeSoloRef = this.afDatabase.list<DadosPlantacao>(`users/${this.userId}/plantacao/umidade_do_solo`);
    return this.umidadeSoloRef;
  }

  //Opção para o usuário ativar um determinado relé de bomba
  ativarRele() {
    this.releRef = this.afDatabase.object(`users/${this.userId}/now/rele`);
    //this.releNow = this.afDatabase.object(`users/${this.userId}/now/rele`).update();
  }

  buscaTemperaturaNow() {
    this.temperaturaNowRef = this.afDatabase.object(`users/${this.userId}/now/temperatura`);
    return this.temperaturaNowRef;
  }

  buscaUmidadeArNow() {
    this.umidadeArNowRef = this.afDatabase.object(`users/${this.userId}/now/umidade_do_ar`);
    return this.umidadeArNowRef;
  }

  buscaUmidadeSoloNow() {
    this.umidadeSoloNowRef = this.afDatabase.object(`users/${this.userId}/now/umidade_do_solo`);
    return this.umidadeSoloNowRef;
  }



  async DefinirDadosGrafico() {
    try {
       await this.temperaturaList$.subscribe(temp => {
        temp.map(h => {
          this.chartLabels.push(h.data);
           this.chartValues.push(h.valor);
        });          
      });
      this.CriarGrafico();
    }
    catch(err) {
      console.log(err);
    }
  }

  CriarGrafico() {
    this.lineChart 	          = new Chart(this.lineCanvas.nativeElement,
      {
         type: 'line',
         data: {
            labels: this.chartLabels,
            datasets: [{
               label                 : 'Últimos dados',
               data                  : this.chartValues,
               duration              : 2000,
               easing                : 'easeInQuart',
               backgroundColor       : 'rgba(152, 54, 145, 0.5)',
               hoverBackgroundColor  : 'rgba(152, 89, 149, 0.5)',
               fill                  : false
            }]
         },
         options : {
            maintainAspectRatio: false,
            legend         : {
               display     : true,
               boxWidth    : 150,
               fontSize    : 15,
               padding     : 0
            },
            scales: {
               yAxes: [{
                  ticks: {
                     beginAtZero:true,
                     stepSize: 20,
                     max : 100
                  }
               }],
               xAxes: [{
                  ticks: {
                     autoSkip: false
                  }
               }]
            }
         }
      });
  }
  

}