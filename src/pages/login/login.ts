import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import { User } from '../../models/user';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	email : string = '';
	senha : string = '';

	user = {} as User;

  constructor(public toast: ToastController ,public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public afAuth: AngularFireAuth) {
  	//this.afAuth.authState.subscribe(userData => {
  	//	console.log(userData);
  //	});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async login(user: User) {
	  try {
			const resultado = await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.senha);
			console.log(resultado);
		if(resultado) {
			this.navCtrl.setRoot(HomePage);
		} else {
			this.toast.create({
				message: `Email ou Senha inválido`,
				duration: 3000
				}).present();
		}
			//Inserir Banner informando que não existe conta
	  }
	  catch(e) {
		 console.error(e);
	  }
	}
	
	googleLogin() {
		this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
		.then(user => {
  		console.log(user);
  	}).catch(err => {
  		alert(err.message);
	  })	
	}

  signup(form) {
  	this.afAuth.auth.createUserWithEmailAndPassword(form.value.email, form.value.senha)
  	.then(user => {
  		console.log(user);
  	}).catch(err => {
  		alert(err.message);
  	})

	}
	
	logout() {
		this.afAuth.auth.signOut().then(data => {
			console.log(data);
		}).catch(err=> {
			alert(err.message);
		})
	}

	registrar() {
		this.navCtrl.push(SignupPage);
	}
}
