import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from '../../models/userProfile';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import {HomePage } from "../home/home";
/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  profile = {} as Profile;

  constructor(private afDatabase: AngularFireDatabase, private afAuth: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
    this.afAuth.authState.subscribe(auth => {
      this.profile.email = auth.email;
    })
  }

  cadastrarPerfil() {
    this.afAuth.authState.take(1).subscribe(auth => {
      this.afDatabase.object(`users/${auth.uid}`).set(this.profile)
        .then(()=> this.navCtrl.setRoot(HomePage));
    });
  }

}
