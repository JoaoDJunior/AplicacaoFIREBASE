import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlantacaoPage } from './plantacao';

@NgModule({
  declarations: [
    PlantacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(PlantacaoPage),
  ],
})
export class PlantacaoPageModule {}
