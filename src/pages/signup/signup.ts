import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabase} from 'angularfire2/database-deprecated';
import {FirebaseListObservable} from 'angularfire2/database-deprecated';
import { User } from '../../models/user';
import { PerfilPage } from '../perfil/perfil';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  user = {} as User;
  email : string = '';
  senha : string = '';
  
  arrData = []

  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public afDatabase: AngularFireDatabase) {
  //  this.afDatabase.list("/users/").subscribe(dados => {
  //    this.arrData = dados;
  //    console.log(this.arrData);
  //  });
  }

  adicionarUsuario() {
   // this.afDatabase.list("/users/").push(this.email);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  async registrar(user: User) {
    try {
      const resultado = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.senha);
      if(resultado) {
        this.navCtrl.push(PerfilPage);
      } else {

      }
      console.log(resultado);
    }
    catch(e) {
      console.error(e);
    }
	}

}
