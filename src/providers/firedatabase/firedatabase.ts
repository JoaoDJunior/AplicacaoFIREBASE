import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';

/*
  Generated class for the FiredatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FiredatabaseProvider {

  constructor(public afd: AngularFireDatabase) {
  }

  getUserData() {
    return this.afd.list('/users/user/');
  }
  setUserData(user) {
    this.afd.list('users/user/').push(user);
  }

}
