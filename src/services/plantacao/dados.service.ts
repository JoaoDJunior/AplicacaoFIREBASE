import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import { DadosPlantacao } from "../../models/DadosPlantacao";
import { AngularFireAuth } from "angularfire2/auth";
import { ToastController } from "ionic-angular/components/toast/toast-controller";


@Injectable()
export class DadosService {

    private user;
    private temperaturaRef;

    constructor(private db: AngularFireDatabase,
                private afAuth: AngularFireAuth,
            private toast: ToastController) {        

    }

    getTemperatura(dados:String) {
        this.temperaturaRef = this.db.list<DadosPlantacao>(`users/${dados}/plantacao/temperatura`);
        console.log(this.user);
        return this.temperaturaRef;
    }

    addTemperatura(temperatura: DadosPlantacao) {
        return this.temperaturaRef.push(temperatura);
    }

    checkReferencia() {
    }
}